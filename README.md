# Backend JavaScript Chapter 8 Challenge

Challenge for Chapter 8 from Binar Academy - Backend JavaScript class

### Prerequisites

1. [Git](https://git-scm.com/downloads)
    ```
    git --version
    ```
2. [Node.js](https://nodejs.org/en/)
    ```
    node -v
    ```
3. [PostgreSQL](https://www.postgresql.org/download/)
    ```
    psql --version
    ```
4. [Heroku](https://devcenter.heroku.com/articles/heroku-cli)
    ```
    heroku version
    ```

### How To Run In Local

1. Clone the repository
    ```
    git clone https://gitlab.com/NaufalK25/backend-javascript-chapter-6-challenge.git
    ```
2. Install dependencies
    ```
    npm install
    yarn
    ```
3. Create a database
    ```
    npm run db-init
    yarn run db-init
    ```
4. Run the server
    ```
    npm run dev
    yarn run dev
    ```

Status: `Deployed`
